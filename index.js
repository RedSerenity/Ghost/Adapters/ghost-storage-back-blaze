'use strict';
const web = require('https');
const path = require('path');
const readFileAsync = require('fs/promises').readFile;
const StorageBase = require('ghost-storage-base');
const B2 = require('backblaze-b2');

class BackBlazeStorageAdapter extends StorageBase {
	options;
	client;
	authTimeStamp = 0;

	downloadHost = null;
	downloadUrl = '/';

	bucket = {
		id: null,
		name: null
	};

	constructor(options) {
		super();

		this.options = options;

		if (!this.options?.keyId || !this.options?.secretKey) {
			throw Error('Invalid configuration');
		}

		this.client = new B2({
			applicationKeyId: this.options.keyId,
			applicationKey: this.options.secretKey
		});
	}

	async handleAuth() {
		try {
			if (this.authTimeStamp != null && this.authTimeStamp > 0) {
				const oneDayAgo = Date.now() - (1000 * 60 * 60 * 23);

				// Is existing token less than 24 hours?
				if (this.authTimeStamp > oneDayAgo) {
					return;
				}
			}

			this.authTimeStamp = 0;
			this.bucket.id = null;
			this.bucket.name = null;
			this.downloadHost = null;
			this.downloadUrl = '';

			const result = await this.client.authorize();

			this.authTimeStamp = Date.now();
			this.bucket.id = result.data.allowed.bucketId;
			this.bucket.name = result.data.allowed.bucketName;
			this.downloadHost = result.data.downloadUrl;
			this.downloadUrl = `${result.data.downloadUrl}/file/${this.bucket.name}`;
		} catch (e) {
			console.error('Unable to AUTHORIZE BackBlaze. Check your config. Error was `%s`', e.response?.data?.message);
		}
	}

	async list() {
		await this.handleAuth();

		return this.client.listFileNames({
			bucketId: this.bucket.id
		}).then((response) => response.data);
	}

	async exists(fileName, targetDir) {
		await this.handleAuth();

		let filePath = path.join(targetDir || this.getTargetDir(), fileName);

		if (filePath[0] === '/') {
			filePath = filePath.substring(1);
		}

		const urlTarget = `${this.downloadUrl}/${filePath}`;
		return new Promise((resolve, reject) => {
			try {
				const request = web.request(urlTarget, { method: 'GET' }, (response) => {
					response.on('error', (err) => reject(err));
					response.on('data', () => {
					});
					response.on('end', () => {
						if (response.statusCode >= 200 && response.statusCode <= 299) {
							resolve(true);
							return;
						}

						if (response.statusCode >= 400 && response.statusCode <= 499) {
							resolve(false);
							return;
						}

						reject(response.errored);
					});
				});
				request.on('error', (err) => reject(err));
				request.end();
			} catch (e) {
				console.error('Exists threw an exception', e);
			}
		});
	}

	async save(image, targetDir) {
		await this.handleAuth();

		return new Promise(async (resolve, reject) => {
			const filePath = targetDir || this.getTargetDir();

			const file = await readFileAsync(image.path);
			const uniqueFileName = await this.getUniqueFileName(image, filePath);

			this.client.getUploadUrl(this.bucket.id)
				.then((location) => {
					this.client.uploadFile({
						uploadUrl: location.data.uploadUrl,
						uploadAuthToken: location.data.authorizationToken,
						filename: uniqueFileName,
						data: file
					}).then(() => resolve(`${this.downloadUrl}/${uniqueFileName}`))
						.catch(reject);
				}).catch(reject);
		});
	}

	serve() {
		return (req, res, next) => { next(); };
	}

	async delete(fileName, targetDir) {
		await this.handleAuth();

		let filePath = path.join(targetDir || this.getTargetDir(), fileName);

		if (filePath[0] === '/') {
			filePath = filePath.substring(1);
		}

		return this.client.listFileVersions({
			bucketId: this.bucket.id,
			startFileName: filePath
		}).then((response) =>
			response.data.files.filter((file) => file.fileName === filePath)
		).then((files) => {
			if (files.length !== 1) {
				return false;
			}

			return this.client.deleteFileVersion({
				fileId: files[0].fileId,
				fileName: files[0].fileName
			}).then(_ => true)
				.catch(_ => false);
		});
	}

	read(options) {}
}

module.exports = BackBlazeStorageAdapter;
