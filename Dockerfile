ARG GHOST_VERSION
FROM ghost:${GHOST_VERSION}
ARG GHOST_VERSION
ARG BUILD_DATE
ARG VCS_REF
ARG BUILD_VERSION

LABEL ghost.version=$GHOST_VERSION
LABEL ghost.os="alpine"
LABEL ghost-storage-back-blaze.version=$BUILD_VERSION
LABEL org.opencontainers.image.created=$BUILD_DATE
LABEL org.opencontainers.image.title="redserenity/ghost-b2"
LABEL org.opencontainers.image.description="A storage adapter for Ghost (https://ghost.org) that supports BackBlaze B2 (https://www.backblaze.com/b2/cloud-storage.html) S3 compatible buckets."
LABEL org.opencontainers.image.url="https://redserenity.com/"
LABEL org.opencontainers.image.source="https://gitlab.com/RedSerenity/Ghost/Adapters/ghost-storage-back-blaze"
LABEL org.opencontainers.image.revision=$VCS_REF
LABEL org.opencontainers.image.vendor="redserenity"
LABEL org.opencontainers.image.version=$BUILD_VERSION
LABEL org.opencontainers.image.authors="Tyler Andersen <tyler@redserenity.com>"
LABEL org.opencontainers.image.licenses="MIT"

COPY . /usr/var/lib/ghost-storage-back-blaze

RUN cd /usr/var/lib/ghost-storage-back-blaze && \
    yarn install && \
    mkdir -p "$GHOST_INSTALL/content.orig/adapters/storage" && \
    ln -s /usr/var/lib/ghost-storage-back-blaze "$GHOST_INSTALL/content.orig/adapters/storage/ghost-storage-back-blaze"

ENV storage__active="ghost-storage-back-blaze"
