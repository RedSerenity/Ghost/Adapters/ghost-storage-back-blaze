# Ghost Storage Adapter - BackBlaze B2

**_This storage adapter works with Ghost 5.x_**

[![npm (scoped)](https://img.shields.io/npm/v/@redserenity/ghost-storage-back-blaze?style=for-the-badge)](https://www.npmjs.com/package/@redserenity/ghost-storage-back-blaze)
[![Docker Image Size (tag)](https://img.shields.io/docker/image-size/redserenity/ghost-b2/alpine?label=Docker%20Image%20Size&style=for-the-badge)](https://hub.docker.com/r/redserenity/ghost-b2)

## Installation

Via Yarn (Recommended)
```shell
cd <ghost install dir>/current
yarn add ghost-storage-back-blaze@npm:@redserenity/ghost-storage-back-blaze
```

Via NPM
```shell
cd <ghost install dir>/current
npm install --save ghost-storage-back-blaze@npm:@redserenity/ghost-storage-back-blaze
```

Via GIT
```shell
mkdir -p <ghost install dir>/content/adapters/storage
cd <ghost install dir>/content/adapters/storage
git clone git@gitlab.com:RedSerenity/Ghost/Adapters/ghost-storage-back-blaze.git
```

## Configuration
#### Configuration file

Edit your `config.<environment>.js` file

```json
{
  ..., // Existing configuration
  "storage": {
    "active": "ghost-storage-back-blaze",
    "ghost-storage-back-blaze": {
      "keyId": '<Your Key Id Here>',
      "secretKey": '<Your Secret Key Here>'
    }
  }
}
```


#### Using environment variables
| Name                                         | Value             |
|----------------------------------------------|-------------------|
| storage__ghost-storage-back-blaze__keyId     | [your key id]     |
| storage__ghost-storage-back-blaze__secretKey | [your secret key] |


#### Notes
The bucket that gets used is the one tied to your `keyId`. Make sure you create your application key for a specific bucket.


## BackBlaze
Login to your [BackBlaze account](https://secure.backblaze.com/user_signin.htm) and go to [App Keys](https://secure.backblaze.com/app_keys.htm). Create a new application key and be sure to select the bucket this key is for (do not create the key for ALL buckets).

![screenshot1.png](https://gitlab.com/RedSerenity/Ghost/Adapters/ghost-storage-back-blaze/-/raw/master/images/screenshot1.png)
![screenshot2.png](https://gitlab.com/RedSerenity/Ghost/Adapters/ghost-storage-back-blaze/-/raw/master/images/screenshot2.png)
